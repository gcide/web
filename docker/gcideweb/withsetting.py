# -*- coding: utf-8 -*-
# This file is part of GCIDE                                
# Copyright (C) 2012-2024 Sergey Poznyakoff
#
# GCIDE is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GCIDE is distributed in the hope that it will be useful,      
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.   
#
# You should have received a copy of the GNU General Public License
# along with GCIDE.  If not, see <http://www.gnu.org/licenses/>.

from django import template
from django.template import Node, NodeList, Template, Context, Variable
from django.template import TemplateSyntaxError
from django.conf import settings
import re

register = template.Library()

class WithSettingNode(template.Node):
    def __init__(self, dict, nodelist):
        self.dict = dict
        self.nodelist = nodelist

    def __repr__(self):
        return "<WithSettingNode>"

    def render(self, context):
        context.push()
        context.update(self.dict)
        output = self.nodelist.render(context)
        context.pop()        
        return output


def mkdict(name,bits,dict):
    if len(bits) < 3 or bits[1] != "as":
        raise TemplateSyntaxError("%r expected format is 'value as name'" %
                                  name)
    if not bits[2].endswith(','):
        if len(bits) > 3:
            raise TemplateSyntaxError("%r expected format is 'value as name'" %
                                      name)
        dict[bits[2]] = eval('settings.' + bits[0])
    else:
        dict[bits[2][0:-1]] = eval('settings.' + bits[0])
        mkdict(name,bits[3:],dict)

#@register.tag
def do_withsetting(parser, token):
    bits = list(token.split_contents())
    dict = {}
    try:
        mkdict(bits[0], bits[1:], dict)
    except AttributeError as diag:
        raise TemplateSyntaxError("%r: %s " % (bits[0], diag))
    nodelist = parser.parse(('endwithsetting',))
    parser.delete_first_token()
    return WithSettingNode(dict, nodelist)

do_withsetting = register.tag('withsetting', do_withsetting)
